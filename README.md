BuildStream.AppImage
====================

This project aims to use BuildStream and BST-Tools to package BuildStream, and its annoying buildbox dependencies as an AppImage.

The project requires a relatively recent version of BuildStream in order to build. However, if you don't yet have one installed, and are building on a fairly typical linux system; the provided `bst.sh` script will download a pre-built AppImage for you.

A modern version of Linux (5.0+) with support for mounting filesystems in userland is also required, since the latest BuildStream uses fuse3.  
The AppImage assumes `git` is installed on the host.

```bash
./bst.sh build appimage.bst
./bst.sh artifact checkout --directory=checkout appimage.bst
```

Alternatively, you can simply download one of the AppImages from the artifact server;

https://artifact.bst.tools/buildstream.appimage/

Installation
------------

The AppImage will work as a stand-in replacement for the `bst` command and there is no need to formally install it.  
That said, if you want to use it instead of a traditional BuildStream installation, copy the AppImage to somewhere safe, and then create symlinks to it from a directory in the path (I like to use ~/.local/bin).

```bash
chmod +x buildstream*.AppImage
mv buildstream*.AppImage ~/.local/bin/buildstream.AppImage
ln -s buildstream.AppImage ~/.local/bin/bst
ln -s buildstream.AppImage ~/.local/bin/bst-artifact-server
```

If you would like to use the AppImage's version of the buildbox binaries, you can do that using symlinks aswell.

```bash
ln -s buildstream.AppImage ~/.local/bin/buildbox-casd
ln -s buildstream.AppImage ~/.local/bin/buildbox-fuse
ln -s buildstream.AppImage ~/.local/bin/buildbox-run
```

On a Per-Project Basis
----------------------

If you have a BuildStream project and would like to make sure your users will always have an appropreate version of BuildStream available;
simply copy the ./bst.sh script from this repository to your own projects, in the same directory as your project.conf file.

The script will search for a locally installed version of BuildStream and will download a BuildStream AppImage from this project if it can't find one.
Do note that at this time, the script ignores the specifics of what version of BuildStream is installed and what version of BuildStream your project calls for.
